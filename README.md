# kx

An experiment in generating static, yet interactive sites and slide decks in
Rust. (Pronounced "kicks," just like your favorite pair of shoes.)

## License

Copyright 2018 Nicholas Young. All rights reserved. Released under a [3 Clause
BSD License](LICENSE).
